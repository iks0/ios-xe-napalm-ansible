:thumbsup:

**ENVIRONMENT SETUP**
>>>
lab network configuration:

csr1000v-01(172.22.100.122)

configuration in file "get_config_output.txt"

csr1000v-02(172.22.100.123)

configuration in file "get_config_output.txt"
>>>

**CONTROLE MACHINE SETUP**
>>>
centos 7 VM(172.22.100.120):


yum -y install open-vm-tool

yum -y install net-tools

yum -y install epel-release

yum -y install centos-release-scl

yum -y install python27

yum -y install rh-python36

yum -y install ansible

git clone https://github.com/napalm-automation/napalm-ansible.git /home/

git clone https://gitlab.com/iks0/val_ios_xe_demo.git

mv ./val_ios_xe_demo/ansible.cfg /etc/ansible/

mv ./val_ios_xe_demo/hosts /etc/ansible/
>>>

>>>
python27 environment(enable environment via "scl enable python27 bash":

pip install --upgrade pip

pip install --upgrade setuptools

pip install napalm-ansible

>>>

**UTILIZATION**
>>>
run config getter(in py27):

ansible-playbook get_facts.yml -e ansible_python_interpreter=/opt/rh/python27/root/usr/bin/python

run config getter to file for editing(in py27):

ansible-playbook get_facts.yml -e ansible_python_interpreter=/opt/rh/python27/root/usr/bin/python > filename.txt

known issues: get_config module escapes newline character

temporary resolution: scp output file to workstation and find/replace "\n"
>>>

**SOURCE CREDIT**
>>>
https://github.com/ktbyers

https://github.com/napalm-automation/napalm-ansible
>>>

